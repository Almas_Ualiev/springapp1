package kz.example.ganre;

import kz.example.Music;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


public class RockMusic implements Music {
    private List<String> songs = new ArrayList<>();

    {
        songs.add("School's Out");
        songs.add("Led Zeppelin Rock and Roll Remaster");
        songs.add("Sympathy For The Devil");
    }


    @Override
    public List<String> getSong() {
        return songs;
    }
}
