package kz.example.ganre;

import kz.example.Music;

import java.util.ArrayList;
import java.util.List;

public class Hiphop implements Music {
    private List<String> songs = new ArrayList<>();

    {
        songs.add("In da Club");
        songs.add("Polazhenie");
        songs.add("50 Cent");
    }


    @Override
    public List<String> getSong() {
        return songs;
    }

}
