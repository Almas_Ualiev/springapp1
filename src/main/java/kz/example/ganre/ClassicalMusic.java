package kz.example.ganre;

import kz.example.Music;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;



public class ClassicalMusic implements Music {
    private List<String> songs = new ArrayList<>();
    @Value("${musicPlayer.value}")
    private int value;
    @Value("${musicPlayer.name}")
    private String name;


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private ClassicalMusic(){}
    {
        songs.add("simphonia 5");
        songs.add("simphonia 4");
        songs.add("simphonia 3");
    }


    public static ClassicalMusic getClassical(){
        return new ClassicalMusic();
    }

    public void doInit(){
        System.out.println("Init");
    }
    public void doDestroy(){
        System.out.println("Destroy");
    }
    @Override
    public List<String> getSong() {
        return songs;
    }
}
