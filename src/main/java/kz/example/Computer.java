package kz.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


public class Computer {
    private int id;
    private MusicPlayer musicPlayer;

    public Computer(MusicPlayer musicPlayer) {
        this.id=1;
        this.musicPlayer = musicPlayer;
    }
    @PostConstruct
    public void myInit(){
        System.out.println("Start my init");
    }
    @PreDestroy
    public void myDestroy(){
        System.out.println("Finish");
    }

    @Override
    public String toString(){
        return "My computer " + musicPlayer.playMusic();

    }


}
