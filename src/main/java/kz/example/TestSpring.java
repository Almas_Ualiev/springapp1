package kz.example;

import kz.example.config.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
//        Music music = context.getBean("classicalMusic", Music.class);
//
//        MusicPlayer musicPlayer = new MusicPlayer(music);
//
//        musicPlayer.playMusic();
//        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
//        musicPlayer.playMusic();

////        Computer computer = context.getBean("computer",Computer.class);
//
//        System.out.println(computer);

        Computer computer1 = context.getBean("computer",Computer.class);
        Computer computer2 = context.getBean("computer",Computer.class);
        Computer computer3 = context.getBean("computer",Computer.class);

        System.out.println(computer1);
        System.out.println(computer2);
        System.out.println(computer3);

        context.close();

    }
}
