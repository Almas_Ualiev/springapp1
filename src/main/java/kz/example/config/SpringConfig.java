package kz.example.config;


import kz.example.Computer;
import kz.example.Music;
import kz.example.MusicPlayer;
import kz.example.ganre.ClassicalMusic;
import kz.example.ganre.Hiphop;
import kz.example.ganre.RockMusic;
import org.springframework.context.annotation.*;

import java.util.Arrays;
import java.util.List;
@Configuration
@PropertySource("classpath:musicPlayer.properties")
@Scope("prototype")
public class SpringConfig {
    @Bean
    public RockMusic rockMusic(){
        return new RockMusic();
    }
    @Bean
    public ClassicalMusic classical(){
        return ClassicalMusic.getClassical();
    }
    @Bean
    public Hiphop hiphop(){
        return new Hiphop();
    }
    @Bean List<Music> musicList(){
        return Arrays.asList(rockMusic(),classical(),hiphop());
    }
    @Bean
    public MusicPlayer musicPlayer(){

        return new MusicPlayer(musicList());
    }
    @Bean
    public Computer computer(){
        return new Computer(musicPlayer());
    }
}
